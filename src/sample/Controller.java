package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import sample.animation.Shake;


/**
 * Класс, демонстрирующий работу калькулятора
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Controller {


    @FXML
    private TextField textFieldOne;


    @FXML
    private TextField textFieldTwo;


    @FXML
    private TextField outPut;

    /**
     * Сложение
     *
     * @param event
     */
    @FXML
    void add(ActionEvent event) {
        double operandOne;
        double operandSecond;
        if (inPutValidation(textFieldOne) == true) {
            operandOne = Double.parseDouble(textFieldOne.getText());

        } else {
            callingTheAnimation(textFieldOne);
            methods();
            return;
        }

        if (inPutValidation(textFieldTwo) == true) {
            operandSecond = Double.parseDouble(textFieldTwo.getText());

        } else {
            callingTheAnimation(textFieldTwo);
            methods();
            return;
        }
        double result = operandOne + operandSecond;
        setOutPut(result);

    }

    @FXML
    void clear(ActionEvent event) {
        textFieldOne.clear();
        textFieldTwo.clear();
        outPut.clear();
    }

    /**
     * Деление
     *
     * @param event
     */
    @FXML
    void divide(ActionEvent event) {
        double operandOne;
        double operandSecond;
        if (inPutValidation(textFieldOne) == true) {
            operandOne = Double.parseDouble(textFieldOne.getText());

        } else {
            callingTheAnimation(textFieldOne);
            methods();
            return;
        }

        if (inPutValidation(textFieldTwo) == true) {
            operandSecond = Double.parseDouble(textFieldTwo.getText());
            if (operandSecond==0)  outPut.setText("На 0 делить нельзя");

        } else {
            callingTheAnimation(textFieldTwo);
            methods();
            return;
        }
        double result = operandOne / operandSecond;
        setOutPut(result);
    }

    /**
     * Умножение
     *
     * @param event
     */
    @FXML
    void multiply(ActionEvent event) {
        double operandOne;
        double operandSecond;
        if (inPutValidation(textFieldOne) == true) {
            operandOne = Double.parseDouble(textFieldOne.getText());

        } else {
            callingTheAnimation(textFieldOne);
            methods();
            return;
        }

        if (inPutValidation(textFieldTwo) == true) {
            operandSecond = Double.parseDouble(textFieldTwo.getText());

        } else {
            callingTheAnimation(textFieldTwo);
            methods();
            return;
        }
        double result = operandOne * operandSecond;
        setOutPut(result);
    }


    /**
     * Вычитание
     *
     * @param event
     */
    @FXML
    void subtract(ActionEvent event) {
        double operandOne;
        double operandSecond;
        if (inPutValidation(textFieldOne) == true) {
            operandOne = Double.parseDouble(textFieldOne.getText());

        } else {
            callingTheAnimation(textFieldOne);
            methods();
            return;
        }

        if (inPutValidation(textFieldTwo) == true) {
            operandSecond = Double.parseDouble(textFieldTwo.getText());

        } else {
            callingTheAnimation(textFieldTwo);
            methods();
            return;
        }
        double result = operandOne - operandSecond;
        setOutPut(result);
    }


    @FXML
    void initialize() {

    }

    /**
     * Метод, осуществляющий проверку на "пустое поле или нет"
     */
    private void checkForErrors() {
        if (textFieldOne.getText().isEmpty() || textFieldTwo.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(textFieldOne);
            Shake secondOperandAnim = new Shake(textFieldTwo);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            return;
        }
    }

    /**
     * Метод, вызывающий анимацию
     *
     * @param operand одно из двух чисел
     */
    private void callingTheAnimation(TextField operand) {
        Shake operandAnimation = new Shake(operand);
        operandAnimation.playAnimation();
    }

    /**
     * Метод, осуществляющий вывод сообщения, если число некорректно
     *
     * @param result результат операции
     */
    void setOutPut(double result) {
        outPut.setText(String.valueOf(result));
    }

    /**
     * Метод, осуществляющий провеку на возможность преобразовать число
     *
     * @param operand число
     * @return true или false
     */
    public boolean inPutValidation(TextField operand) {
        try {
            Double.parseDouble(operand.getText());
            return true;
        } catch (Exception e) {
            callingTheAnimation(operand);
            return false;
        }
    }

    void methods() {
        checkForErrors();
        outPut.setText("Некорректное число");
        outPut.setStyle("-fx-text-fill: red;");
    }
}

